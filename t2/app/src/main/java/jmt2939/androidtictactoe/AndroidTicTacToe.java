package jmt2939.androidtictactoe;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class AndroidTicTacToe extends ActionBarActivity {

    public void startNewGame(View view) { startNewGame(); }

    private class ButtonClickListener implements View.OnClickListener {
        int location;

        public ButtonClickListener(int location) {
            this.location = location;
        }

        @Override
        public void onClick(View v) {
            if(mGameOver)return;
            if (mBoardButtons[location].isEnabled()) {
                setMove(TicTacToeGame.HUMAN_PLAYER, location);

                //if no winner yet, let the computer make a move
                int winner = mGame.checkForWinner();
                if (winner == 0) {
                    mInfoTextView.setText(R.string.turn_computer);
                    int move = mGame.getComputerMove();
                    setMove(TicTacToeGame.COMPUTER_PLAYER, move);
                    winner = mGame.checkForWinner();
                }
                if (winner == 0) {
                    mInfoTextView.setText(R.string.turn_human);
                } else if (winner == 1) {
                    mInfoTextView.setText(R.string.result_tie); mGameOver = true; ties++;
                } else if (winner == 2) {
                    mInfoTextView.setText(R.string.result_human_wins); mGameOver = true; humanWon++;
                } else {
                    mInfoTextView.setText(R.string.result_computer_wins); mGameOver = true; androidWon++;
                }
                if(winner != 0){
                    TextView h = (TextView) findViewById(R.id.human_wins);
                    TextView a = (TextView) findViewById(R.id.android_wins);
                    TextView t = (TextView) findViewById(R.id.ties);
                    h.setText(getResources().getString(R.string.show_human_wins) + " " + humanWon);
                    a.setText(getResources().getString(R.string.show_android_wins) + " " + androidWon);
                    t.setText(getResources().getString(R.string.show_ties) + " " + ties);
                }
            }
        }
    }

    private void setMove(char player, int location){
        mGame.setMove(player, location);
        mBoardButtons[location].setEnabled(false);
        mBoardButtons[location].setText(String.valueOf(player));
        if(player == TicTacToeGame.HUMAN_PLAYER)
            mBoardButtons[location].setTextColor(Color.rgb(0, 200, 0));
        else
            mBoardButtons[location].setTextColor(Color.rgb(200, 0, 0));
    }


    //number of wins, losses, and ties
    int humanWon;
    int androidWon;
    int ties;

    //default color scheme
    private Boolean darkBackground;

    //boolean in order to alternate who goes first
    private Boolean humanGoesFirst;

    //boolean that says whether the game is over or not
    public Boolean mGameOver;

    //represents the TicTacToe game state
    private TicTacToeGame mGame;

    //Buttons making up the board
    private Button mBoardButtons[];

    //various text displayed
    private TextView mInfoTextView;


    private void startNewGame(){

        mGame.clearBoard();
        mGameOver = false;

        for(int i = 0; i < mBoardButtons.length; i++){
            mBoardButtons[i].setText("");
            mBoardButtons[i].setEnabled(true);
            mBoardButtons[i].setOnClickListener(new ButtonClickListener(i));
        }

        TextView h = (TextView) findViewById(R.id.human_wins);
        TextView a = (TextView) findViewById(R.id.android_wins);
        TextView t = (TextView) findViewById(R.id.ties);
        h.setText(getResources().getString(R.string.show_human_wins) + " " + humanWon);
        a.setText(getResources().getString(R.string.show_android_wins) + " " + androidWon);
        t.setText(getResources().getString(R.string.show_ties) + " " + ties);

        //alternating firsts
        if(humanGoesFirst){ humanGoesFirst = false; mInfoTextView.setText(R.string.first_human); }
        else{
            humanGoesFirst = true;
            mInfoTextView.setText(R.string.first_compy);

            int move = mGame.getComputerMove();
            setMove(TicTacToeGame.COMPUTER_PLAYER, move);
            mInfoTextView.setText(R.string.turn_human);
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        humanGoesFirst = true; darkBackground = true;
        humanWon = 0; androidWon = 0; ties = 0;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mBoardButtons = new Button[TicTacToeGame.BOARD_SIZE];
        mBoardButtons[0] = (Button)findViewById(R.id.one);
        mBoardButtons[1] = (Button)findViewById(R.id.two);
        mBoardButtons[2] = (Button)findViewById(R.id.three);
        mBoardButtons[3] = (Button)findViewById(R.id.four);
        mBoardButtons[4] = (Button)findViewById(R.id.five);
        mBoardButtons[5] = (Button)findViewById(R.id.six);
        mBoardButtons[6] = (Button)findViewById(R.id.seven);
        mBoardButtons[7] = (Button)findViewById(R.id.eight);
        mBoardButtons[8] = (Button)findViewById(R.id.nine);

        mInfoTextView = (TextView) findViewById(R.id.information);

        mGame = new TicTacToeGame();
        startNewGame();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
//        menu.add("New Game");
          menu.add(R.string.light);
          menu.add(R.string.dark);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getTitle() == getResources().getString(R.string.light)){
            View v = findViewById(R.id.main_layout);
            v.setBackgroundColor(Color.parseColor("#ffffffff"));
            View v2 = findViewById(R.id.score_box);
            v2.setBackgroundColor(Color.parseColor("#ff3b3b3b"));
            TextView v3 = (TextView) findViewById(R.id.information);
            v3.setTextColor(Color.parseColor("#FF000000"));
        }else if(item.getTitle() == getResources().getString(R.string.dark)){
            View v = findViewById(R.id.main_layout);
            v.setBackgroundColor(Color.parseColor("#ff383838"));
            View v2 = findViewById(R.id.score_box);
            v2.setBackgroundColor(Color.parseColor("#a16b6b6b"));
            TextView v3 = (TextView) findViewById(R.id.information);
            v3.setTextColor(Color.parseColor("#ffffffff"));
        }
        return true;
    }

}


